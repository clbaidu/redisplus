package com.hong.springboot;

import com.hong.springboot.demo.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author zengzh
 * @date create at 2018/5/16 16:00
 */
@RestController
public class DemoController {

    @Autowired
    DemoService demoService;

    @RequestMapping("/redis")
    public Object testDemo(){
        String id = UUID.randomUUID().toString();
        demoService.testService(id);
        return new String(id);
    }
}
