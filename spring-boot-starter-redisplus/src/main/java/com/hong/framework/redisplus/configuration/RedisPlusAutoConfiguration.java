package com.hong.framework.redisplus.configuration;

import com.hong.framework.redisplus.RedisLockFactory;
import com.hong.framework.redisplus.RedisplusKeyGenerator;
import com.hong.framework.redisplus.aspect.RedisLockAspect;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author zengzh
 * @date create at 2018/5/16 11:23
 */
@Configuration
@AutoConfigureAfter(RedisAutoConfiguration.class)
@ConditionalOnBean(RedisTemplate.class)
@Import(RedisLockAspect.class)
public class RedisPlusAutoConfiguration {

    @Bean
    public RedisLockFactory redisLockFactory() {
        return new RedisLockFactory();
    }

    @Bean
    public RedisplusKeyGenerator redisplusKeyGenerator() {
        return new RedisplusKeyGenerator();
    }

}
